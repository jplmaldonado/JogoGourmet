package dev.jmaldonado.view;

import dev.jmaldonado.controller.Jogo;

/**
 * CLASSE MAIN START DA APLICA��O
 * @author jo�o maldonado
 */
public class JFMain {

	public static void main(String[] args) {
		new Jogo().start();
	}
}
