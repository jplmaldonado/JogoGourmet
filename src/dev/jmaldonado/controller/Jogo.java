package dev.jmaldonado.controller;

import javax.swing.JOptionPane;

import dev.jmaldonado.models.Prato;
import dev.jmaldonado.utils.FrameUtils;

/**
 * CLASSE COM IMPLEMENTACAO DE EVENT LISTENER USADO PELO COMPONENTE JBUTTON PARA
 * ISOLAMENTO DE RESPONSABILIDADES RESPONSAVEL POR CORRER TODOS OS PRATOS E
 * VERIFICAR SE TEM OU N�O CARACTERISTICA CASO USUARIO ERRE TODAS AS PERGUNTAS ,
 * EXIBE A TELA PARA CADASTRO DE UM NOVO PRATO, E COME�A UM NOVO LOOP
 */
public class Jogo {

	BuscaBinaria buscaBinaria;
	boolean loopInfinito = true;

	public Jogo() {
		buscaBinaria = new BuscaBinaria();
	}

	public void start() {

		if (buscaBinaria.estaVazia()) {
			carregarPratosIniciais();
		}

		if (FrameUtils.perguntaInicial() == JOptionPane.CLOSED_OPTION) {
			loopInfinito = false;
		}

		while (loopInfinito) {
			advinhaPrato(buscaBinaria.prato);
		}

	}

	private void carregarPratosIniciais() {
		buscaBinaria.insere(null, "Massa", true);
		buscaBinaria.insere(buscaBinaria.prato, "Lasanha", true);
		buscaBinaria.insere(buscaBinaria.prato, "Bolo de chocolate", false);
	}

	public void advinhaPrato(Prato prato) {

		if (FrameUtils.perguntaPrato(prato)) {
			if (prato.verificaVazio()) {
				FrameUtils.imprimeAcerto();
				start();
			} else {
				advinhaPrato(prato.getPratoDireita());
			}
		} else {
			if (prato.getPratoDireita() == null) {
				FrameUtils.cadastrarNovoPrato(prato);
				start();
			} else {
				advinhaPrato(prato.getPratoEsquerda());
			}
		}
	}

}