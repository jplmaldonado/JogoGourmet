package dev.jmaldonado.controller;

import dev.jmaldonado.models.Prato;

public class BuscaBinaria {

	public Prato prato;

	public void insere(Prato pratoNode, String value, boolean acertou) {
		prato = insereNovoPrato(pratoNode, value, acertou);
	}

	public boolean estaVazia() {
		return prato == null;
	}

	private Prato insereNovoPrato(Prato pratoNode, String value, boolean acertou) {
		
		if (pratoNode == null) {
			pratoNode = new Prato(value);
			return pratoNode;
		} else if (acertou) {
			pratoNode.setPratoDireita(insereNovoPrato(pratoNode.getPratoDireita(), value, acertou));
		} else {
			pratoNode.setPratoEsquerda(insereNovoPrato(pratoNode.getPratoEsquerda(), value, acertou));
		}

		return pratoNode;
	}

}
