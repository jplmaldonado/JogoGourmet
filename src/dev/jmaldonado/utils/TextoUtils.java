package dev.jmaldonado.utils;

/**
 * CLASSE UTILS PARA ARMAZENAR OS TEXTOS DA APLICA��O
 * E ALGUNS PARAMETROS DE LARGURA E ALTURA
 * @author jo�o maldonado
 */
public class TextoUtils {

	public static final String FRASE_INICIAL = "Pense em um prato que gosta";
	public static final String TITULO_JANELA_1 = "Confirm";
	public static final Integer WIDTH = 300;
	public static final Integer HEIGHT = 120;
	public static final String ACERTO = "Acertei de novo !!!";
	public static final String PEGUNTA_PRATO = "O prato que voc� pensou � ";
	public static final String QUAL_PRATO_PENSOU = "Qual prato voc� pensou ?";
	public static final String PRATO_NAO_INFORMADO = "ERRO! Prato n�o informado";
	public static final String CARACTERISTICA_NAO_INFORMADA = "ERRO! Caracteristica n�o informada";
	
}
