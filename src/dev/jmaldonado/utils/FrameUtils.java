package dev.jmaldonado.utils;

import javax.swing.JOptionPane;

import dev.jmaldonado.models.Prato;

/**
 * CLASSE UTILS PARA EXIBICAO DE PERGUNTAS NA TELA E CADASTRO DE NOVO PRATO
 * 
 * @author Jo�o Maldonado
 *
 */
public class FrameUtils {

	public static void imprimeAcerto() {
		JOptionPane.showMessageDialog(null, TextoUtils.ACERTO);
	}

	public static boolean perguntaCaracteristica(Prato prato) {
		return JOptionPane.showConfirmDialog(null, TextoUtils.PEGUNTA_PRATO.concat(prato.getPrato()).concat("?"),
				TextoUtils.TITULO_JANELA_1, JOptionPane.YES_NO_OPTION) == 0 ? true : false;
	}

	public static boolean perguntaPrato(Prato prato) {	
		return JOptionPane.showConfirmDialog(null, TextoUtils.PEGUNTA_PRATO.concat(prato.getPrato()).concat("?"),
				TextoUtils.TITULO_JANELA_1, JOptionPane.YES_NO_OPTION) == 0 ? true : false;
	}

	public static void cadastrarNovoPrato(Prato pratoNode) {
		String nome = JOptionPane.showInputDialog(TextoUtils.QUAL_PRATO_PENSOU);
		String caracteristica = JOptionPane.showInputDialog(nome + " � ______ mas o " + pratoNode.getPrato() + " n�o");
		String pratoAtual = pratoNode.getPrato();
		pratoNode.setPrato(caracteristica);
		pratoNode.setPratoEsquerda(new Prato(pratoAtual));
		pratoNode.setPratoDireita(new Prato(nome));
	}

	public static int perguntaInicial() {
		Object[] options = { "OK" };
		return JOptionPane.showOptionDialog(null, TextoUtils.FRASE_INICIAL, TextoUtils.TITULO_JANELA_1,
				JOptionPane.PLAIN_MESSAGE, JOptionPane.QUESTION_MESSAGE, null, options, options[0]);
	}

}