package dev.jmaldonado.models;

public class Prato {

	private String prato;
	private Prato pratoEsquerda;
	private Prato pratoDireita;

	public Prato(String prato) {
		this.prato = prato;
	}

	public boolean verificaVazio() {
		return pratoEsquerda == null && pratoDireita == null;
	}

	public String getPrato() {
		return prato;
	}

	public void setPrato(String prato) {
		this.prato = prato;
	}

	public Prato getPratoEsquerda() {
		return pratoEsquerda;
	}

	public void setPratoEsquerda(Prato pratoEsquerda) {
		this.pratoEsquerda = pratoEsquerda;
	}

	public Prato getPratoDireita() {
		return pratoDireita;
	}

	public void setPratoDireita(Prato pratoDireita) {
		this.pratoDireita = pratoDireita;
	}
}
